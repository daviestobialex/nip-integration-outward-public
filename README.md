# Open Source NIBSS Integration Outward Service

# Purpose 

This project exposes Endpoints for communication with the NIBSS Service.

# Stack

![](https://img.shields.io/badge/mysql-✓-blue.svg) 
![](https://img.shields.io/badge/java_8-✓-blue.svg)
![](https://img.shields.io/badge/spring_boot-✓-blue.svg)
![](https://img.shields.io/badge/swagger_2-✓-blue.svg)
![](https://img.shields.io/badge/jaxb-✓-blue.svg)
![](https://img.shields.io/badge/octopus_ssm_common-✓-blue.svg)


The following Technology stack was used in this project :

**Java 8** : Core language used

**SpringBoot 2.0** : Displays the REST API

**MySql 5.5** : Database access

**jaxb** : This helps convert xml string to an object and vice-versa. This library comes along with java 8 and below but for user with java 8 and above you would have to import the library in the `pom.xml`

**octopus_ssm_common** : Custom External inbuilt library for encrypting and decrypting xml string to the NIBSS standard

# Steps To Run

**1** - Do a git clone of the project : 

**2** - Open application.properties , set the default profile to run { dev / live / uat }

**3** - Edit the configuration of application-{profile} to suit the appropriate environment eg port , database url , database username & password

**4** - Run the project from the IDE 

**5** - Check the docs folder for the Integration document.


# Swagger Url

Swagger is used to expose the rest Endpoints via the following link : 

**http://{host}:{port}/outward/swagger-ui.html** 


# Package Structure  - org.agency.nip.service

**1** - contoller : This package contains the endpoints that client calls to the rest service . The ClientApi class contains string repressentations of the endpoint url

**2** - mapper :This package contains classes that are used to build the xml payload string and also to parse  the response obtained from NIBSS

**3** - config : This package contains classes for managing configuration values in the config folder and different scoped beans which the application uses .

**4** - controller : This package contains controllers that exposes the rest Endpoints for the client.

**5** - model: This package contains generic objects to help pass the NIBSS payload to the client and also contain custom request and response objects that is to be passed and read from client

**8** - entity : This package contains entity classes 

**8** - exception.handler : This package handles generic exception handlers for error management, ideally customer exceptions are to be placed under the exceptions package but no custom exceptions were determined as at the completion of this project

**9** - facade : This package contains classes that are called by the Controller classes . It exposes methods that handles all processing of the incoming coming data from client and received responses from NIBSS

**10** - repository : This package exposes business methods (interfaces) for entitites using SpringRepository

**11** - enums : This package maps the response code from NIBSS to Custom Response code 



 # SIDE NOTE 
**1** - NIBSS pojo's ar generated from the NIBSS wsdl check the pom for maven configuration which enables that

**2** - `RSAKeyPairGenerator.java` is used in generating the NIBSS public and private key pair, it is a bit manual for now, until I have the time to auto generate based on configuration.

**3** - check the test packages for tests written for session id generator and the encrypt and decrypt basics

## HAPPY CODING !! ##

