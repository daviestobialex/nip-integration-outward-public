package com.agency.nip.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NipServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NipServiceApplication.class, args);
	}

}
