/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @author odavies
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties
@PropertySource("file:./config/configs-${spring.profiles.active}.properties")
public class BaseConfig {

    private String ftBaseUrl;

    private String tsqBaseUrl;

    private String publickeystore;

    private String storage;

    private String keypass;

    private String keyGroup;

    private String privatekey;

    private String publicKey;

    private String publicKeySanef;
    
    private String institutioncode;

}
