/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.config;

import com.agency.nip.service.connectors.SOAPConnector;
import com.agency.octopus.ssm.common.PGP_SSM;
import org.springframework.context.annotation.Bean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 *
 * @author ailori
 */
@Slf4j
@Configuration
public class BeansConfig extends BaseConfig {

    @Bean
    public PGP_SSM PGP_SSM() {
        PGP_SSM pgp_ssm = new PGP_SSM();
        pgp_ssm.setPublicKeyLocation(getPublicKeySanef());
        pgp_ssm.setPrivateKeyLocation(getPrivatekey());
        pgp_ssm.setPrivateKeyPassPhrase(getKeypass());
        return pgp_ssm;
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        // this is the package name specified in the <generatePackage> specified in
        // pom.xml
        marshaller.setContextPath("com.agency.nip.service.models.ft.wsdl");
        return marshaller;
    }

    @Bean
    public SOAPConnector soapConnector(Jaxb2Marshaller marshaller) {
        SOAPConnector client = new SOAPConnector();
        client.setDefaultUri("http://196.6.103.10:86/NIPWS/NIPInterface");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
