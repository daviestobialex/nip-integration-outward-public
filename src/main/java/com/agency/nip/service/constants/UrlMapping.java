/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.constants;

/**
 *
 * @author odavies
 */
public interface UrlMapping {

    public final String ACCESS_IN_ROOT_MAP = "v1/in";

    public final String ACCESS_OUT_ROOT_MAP = "v1/out";

    public final String NE_OUT_URL = "/nameenquiry/{code}/{channel}/{accountNumber}";

    public final String NE_IN_URL = "/nameenquiry";

    public final String BE_OUT_URL = "/balanceenquiry/{code}/{channel}/{accountNumber}";

    public final String BE_IN_URL = "/balanceenquiry";

    public final String ACCOUNT_BLOCK_URL = "/account/block";

    public final String ACCOUNT_UNBLOCK_URL = "/account/unblock";

    public final String AMOUNT_BLOCK_URL = "/amount/block";

    public final String AMOUNT_UNBLOCK_URL = "/amount/unblock";
}
