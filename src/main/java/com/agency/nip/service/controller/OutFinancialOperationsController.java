/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.controller;

import static com.agency.nip.service.constants.UrlMapping.ACCESS_OUT_ROOT_MAP;
import static com.agency.nip.service.constants.UrlMapping.NE_OUT_URL;
import com.agency.nip.service.facade.OutwardFinancialOperationsFacade;
import com.agency.nip.service.models.requests.FTRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author odavies
 */
@RestController
@RequestMapping(ACCESS_OUT_ROOT_MAP)
public class OutFinancialOperationsController {

    @Autowired
    private OutwardFinancialOperationsFacade financialOperationsFacade;

    @ResponseBody
    @GetMapping(NE_OUT_URL)
    public ResponseEntity nameEnquiry(@Valid @PathVariable String code, @Valid @PathVariable String channel, @Valid @PathVariable String accountNumber) {
        return ResponseEntity.ok().body(financialOperationsFacade.nameEnquiry(code, channel, accountNumber));
    }

    @ResponseBody
    @PostMapping("ft/credit")
    public ResponseEntity ftDirectCredit(@Valid @RequestBody FTRequest request) {
        return ResponseEntity.ok().body(financialOperationsFacade.directCredit(request));
    }

    @ResponseBody
    @GetMapping("tsq/{trnasactionRef}")
    public ResponseEntity tsq(@Valid @PathVariable String trnasactionRef) {
        return ResponseEntity.ok().body(financialOperationsFacade.tsq(trnasactionRef));
    }
}
