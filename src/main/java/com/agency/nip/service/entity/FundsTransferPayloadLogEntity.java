/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author odavies
 */
@Data
@NoArgsConstructor
@Entity
public class FundsTransferPayloadLogEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "funds_transfer_id")
    private NIPFundsTransferEntity fundsTransactionEntity;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String requestPayload;

    private Date requestTime;

    @Column(columnDefinition = "TEXT", nullable = true)
    private String responsePayload;

    private Date responseTime;

    @CreationTimestamp
    private Timestamp dateCreated;

    @Column(insertable = false, updatable = true)
    @UpdateTimestamp
    private Timestamp lastModified;

}
