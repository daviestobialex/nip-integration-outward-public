/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author odavies
 */
//@lombok.Data
//@Entity
//public class NIPAmountAcountBlockUnblockEntity extends AmountOrAccountBlockOrUnblockRequest implements Serializable {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    /**
//     * Unblock id returned from MIFOS
//     */
//    private String unblockId;
//
//    @CreationTimestamp
//    private Timestamp dateCreated;
//
//    @Column(insertable = false, updatable = true)
//    @UpdateTimestamp
//    private Timestamp lastModified;
//}
