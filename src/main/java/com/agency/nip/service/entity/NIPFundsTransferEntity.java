/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author odavies
 */
@lombok.Data
@Entity
public class NIPFundsTransferEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, length = 50, nullable = false)
    private String requestId;

    @Column(unique = true, length = 50, nullable = true)
    private String transactionRef;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "fundsTransactionEntity")
    private FundsTransferPayloadLogEntity fundsTransferPayloadLogEntity;

    private char processStatus = 'I';

    @Column(unique = false, length = 5, nullable = true)
    private String responseCode;

    @Column(nullable = true, unique = true, length = 50)
    protected String paymentReference;

    @Lob
    protected String narration;

    @Column(length = 150)
    protected String originatorAccountNumber;

    @Column(length = 50)
    protected String originatorBankVerificationNumber;

    @Column(length = 20)
    protected String amount;

    @Column(nullable = false, unique = true, length = 50)
    protected String nameEnquiryRef;

    @Column(length = 10)
    protected String beneficiaryKYCLevel;

    @Column(length = 5)
    protected String channelCode;

    @Column(length = 21)
    protected String originatorKYCLevel;

    @Column(length = 20)
    protected String destinationInstitutionCode;

    @Column(length = 20)
    protected String beneficiaryAccountNumber;

    @Column(length = 50)
    protected String originatorAccountName;

    @Column(length = 20)
    protected String beneficiaryBankVerificationNumber;

    @Column(length = 30)
    protected String transactionLocation;

    @Column(length = 50)
    protected String beneficiaryAccountName;

    @Column(nullable = false, unique = true, length = 50)
    protected String sessionID;

    @CreationTimestamp
    private Timestamp dateCreated;

    @Column(insertable = false, updatable = true)
    @UpdateTimestamp
    private Timestamp lastModified;
}
