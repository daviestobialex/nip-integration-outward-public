package com.agency.nip.service.exceptions.handlers;

import java.io.IOException;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.inlaks.dto.BaseResponse;
import org.inlaks.responsecode.InlaksResponseCodeEnum;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpClientErrorException;

@NoArgsConstructor
@Slf4j
public class ConnectionExceptionHandler extends DefaultResponseErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(HttpClientErrorException.class)
    public BaseResponse handleConflict(HttpClientErrorException e) {
        log.info("Exception :::::::{}", e.getResponseBodyAsString());
        log.info("Response Headers Object :::: {}", e.getResponseHeaders());
        log.info("Raw Status Code  :::: {}", e.getRawStatusCode());
        log.info("Status Text :::: {}", e.getStatusText());

//        paymentDataService.log(e.getResponseBodyAsString(), chargeCustomer.getEmail(), chargeCustomer.getAuthorization_code());
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription("Error Sending Request");
        return baseResponse;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        // your error handling here
        log.info("Exception :::::::{}", response.getBody().toString());
        log.info("Raw Status Code  :::: {}", response.getRawStatusCode());
        log.info("Status Text :::: {}", response.getStatusText());
    }
}
