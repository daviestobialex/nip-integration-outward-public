package com.agency.nip.service.exceptions.handlers;

import java.lang.annotation.AnnotationFormatError;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import static java.util.stream.Collectors.joining;
import org.hibernate.exception.ConstraintViolationException;
import org.inlaks.dto.BaseResponse;
import org.inlaks.responsecode.InlaksResponseCodeEnum;

@Slf4j
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GenericExceptionHandler {

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<BaseResponse> handleException(AnnotationFormatError ex) {
        log.error("AnnotationFormatError Invalid Request ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<BaseResponse> handleException(ConstraintViolationException ex) {
        log.error("Constraint Violation Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<BaseResponse> handleException(DataIntegrityViolationException ex, WebRequest request) {
        log.error("DataIntegrity Violation Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SocketTimeoutException.class)
    public ResponseEntity<BaseResponse> handleException(SocketTimeoutException ex) {
        log.error("Socket Timeout Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_63.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_63.getResponseDescription());

        return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<BaseResponse> handleException(HttpMediaTypeNotSupportedException ex) {
        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append(ex.getContentType()).append(" media type is not supported. Supported media types are ").
                append(ex.getSupportedMediaTypes().stream().map(MediaType::getType).collect(joining(",")));
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        val format = String.format(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription(), messageBuilder.toString());

        return new ResponseEntity<>(baseResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UnknownHostException.class)
    public ResponseEntity<BaseResponse> handleException(UnknownHostException ex) {
        log.error("Unknown Host Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());
        return new ResponseEntity<>(baseResponse, HttpStatus.ACCEPTED);
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<BaseResponse> handleException(NumberFormatException ex) {
        log.error("Number Format Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription() + ", check number format");
        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<BaseResponse> handleException(IllegalArgumentException ex) {
        log.error("Illegal Argument Exception ::: {}", ex);
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setResponseCode(InlaksResponseCodeEnum.RESPONSE_01.getResponseCode());
        baseResponse.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_01.getResponseDescription());
        return new ResponseEntity<>(baseResponse, HttpStatus.BAD_REQUEST);
    }

}
