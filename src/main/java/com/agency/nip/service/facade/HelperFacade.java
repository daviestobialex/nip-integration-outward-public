/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.facade;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 *
 * @author t.davies
 */
@Slf4j
@Component
public class HelperFacade {

    /**
     * Converts an original text to base 64 string
     *
     * @param originalInput
     * @return
     */
    public String ToBase64Encode(String originalInput) {
        return Base64.getEncoder().encodeToString(originalInput.getBytes());
    }

    /**
     *
     * @param originalInput
     * @return
     */
    public String ToBase64Encode(byte[] originalInput) {
        return Base64.getEncoder().withoutPadding().encodeToString(originalInput);
    }

    /**
     * Converts an bas64 text to original text
     *
     * @param base64Text
     * @return
     */
    public String FromBase64Decode(String base64Text) {
        byte[] decodedBytes = Base64.getDecoder().decode(base64Text);
        return new String(decodedBytes);
    }

    /**
     * Converts an bas64 text to original text
     *
     * @param base64Text
     * @return
     */
    public String FromBase64MimeDecode(String base64Text) {
        byte[] decodedBytes = Base64.getMimeDecoder().decode(base64Text);
        return new String(decodedBytes);
    }

    /**
     * Converts text to a hashed string
     *
     * @param text
     * @return
     */
    public String HashString256(String text) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(text.getBytes(StandardCharsets.UTF_8));
            // System.out.println("Compare this = " + ToBase64Encode(hash));
            return ToBase64Encode(hash);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HelperFacade.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

//    public String encrypt(String value) {
//        try {
//            IvParameterSpec iv = new IvParameterSpec(getIvKey().getBytes("UTF-8"));
//            SecretKeySpec skeySpec = new SecretKeySpec(getApiKey().getBytes("UTF-8"), "AES");
//
//            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
//
//            byte[] encrypted = cipher.doFinal(value.getBytes());
////            System.out.println(new String(encrypted));
////            System.out.println("===========================================");
//            return Base64.getEncoder().withoutPadding().encodeToString(encrypted);
////            return encodeHexString(encrypted);
//        } catch (UnsupportedEncodingException | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
//    public String decrypt(String encrypted) {
//        try {
//            IvParameterSpec iv = new IvParameterSpec(getIvKey().getBytes("UTF-8"));
//            SecretKeySpec skeySpec = new SecretKeySpec(getApiKey().getBytes("UTF-8"), "AES");
//
//            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
//            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
//            byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));
//
////            byte[] original = cipher.doFinal(decodeHexString(encrypted));
//            return new String(original, "UTF-8");
//        } catch (UnsupportedEncodingException | InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
//            ex.printStackTrace();
//        }
//
//        return null;
//    }
    /**
     * Generates Application Id Number for Merchant SDK
     *
     * @return
     */
    public String generateApplicationId() {
        return UUID.randomUUID().toString();
    }

    public String encodeHexString(byte[] byteArray) {
        StringBuffer hexStringBuffer = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            hexStringBuffer.append(byteToHex(byteArray[i]));
        }
        return hexStringBuffer.toString();
    }

    public String byteToHex(byte num) {
        char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    public byte hexToByte(String hexString) {
        int firstDigit = toDigit(hexString.charAt(0));
        int secondDigit = toDigit(hexString.charAt(1));
        return (byte) ((firstDigit << 4) + secondDigit);
    }

    private int toDigit(char hexChar) {
        int digit = Character.digit(hexChar, 16);
        if (digit == -1) {
            throw new IllegalArgumentException(
                    "Invalid Hexadecimal Character: " + hexChar);
        }
        return digit;
    }

    public byte[] decodeHexString(String hexString) {
        if (hexString.length() % 2 == 1) {
            throw new IllegalArgumentException(
                    "Invalid hexadecimal String supplied.");
        }

        byte[] bytes = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length(); i += 2) {
            bytes[i / 2] = hexToByte(hexString.substring(i, i + 2));
        }
        return bytes;
    }

    public String HashString512(String input) {
        try {
            // getInstance() method is called with algorithm SHA-512 
            MessageDigest md = MessageDigest.getInstance("SHA-512");

            // digest() method is called 
            // to calculate message digest of the input string 
            // returned as array of byte 
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value 
            String hashtext = no.toString(16);

            // Add preceding 0s to make it 32 bit 
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

            // return the HashText 
            return hashtext;
        } // For specifying wrong message digest algorithms 
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Converts text to a hashed string
     *
     * @param text
     * @return
     */
    public String HashString256Hex(String text) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(text.getBytes(StandardCharsets.UTF_8));
            // System.out.println("Compare this = " + ToBase64Encode(hash));
            // Convert byte array into signum representation 
            BigInteger no = new BigInteger(1, hash);

            // Convert message digest into hex value 
            String hashtext = no.toString(16);

            // Add preceding 0s to make it 32 bit 
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

            // return the HashText 
            return hashtext;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HelperFacade.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    public String HashString256Hex2(String text) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(text.getBytes(StandardCharsets.UTF_8));
            // System.out.println("Compare this = " + ToBase64Encode(hash));

            return encodeHexString(hash);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(HelperFacade.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    /**
     * Generates a 6 digit Otp Code
     *
     * @return
     */
    public String getOtpCode() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        // this will convert any number sequence into 6 character.
        return String.format("%06d", new Random().nextInt(999999));
    }

    public String getCompanyCode() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        // this will convert any number sequence into 6 character.
        return String.format("%04d", new Random().nextInt(9999));
    }

    public String generateSessionID(String code) {
        return code.concat(this.getCurrentDate("yyMMddHHmmss")).concat(this.getRandomNumber(12));
    }

    public String getRandomNumber(int length) {
        String AB = "0123456789";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }

    public String getSeatId(int length) {
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString().toUpperCase();
    }

    public String generateDiscountPercent() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        // this will convert any number sequence into 6 character.
        return String.format("%02d", new Random().nextInt(99));
    }

    public String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
//        System.out.println("==== DATE == " + dateFormat.format(date));
        return dateFormat.format(date);
    }

    public String getCurrentDate(String pattern) {
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        Date date = new Date();
//        System.out.println("==== DATE == " + dateFormat.format(date));
        return dateFormat.format(date);
    }

    public Date getDate(String date) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
            return dateFormat.parse(date);
        } catch (ParseException pe) {
            return null;
        }
    }

    public String getCurrentDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
//        System.out.println("==== DATE == " + dateFormat.format(date));
        return dateFormat.format(date);
    }

}
