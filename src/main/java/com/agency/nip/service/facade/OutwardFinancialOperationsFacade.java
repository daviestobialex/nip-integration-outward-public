/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.facade;

import com.agency.nip.service.config.BaseConfig;
import com.agency.nip.service.connectors.SOAPConnector;
import com.agency.nip.service.entity.FundsTransferPayloadLogEntity;
import com.agency.nip.service.entity.NIPFundsTransferEntity;
import com.agency.nip.service.mapper.GeneralMapper;
import com.agency.nip.service.mapper.XmlBuilder;
import com.agency.nip.service.models.enums.NIPResponseCodes;
import com.agency.nip.service.models.ft.wsdl.FTSingleCreditRequest;
import com.agency.nip.service.models.ft.wsdl.FTSingleCreditResponse;
import com.agency.nip.service.models.ft.wsdl.FundtransfersingleitemDc;
import com.agency.nip.service.models.ft.wsdl.FundtransfersingleitemDcResponse;
import com.agency.nip.service.models.ft.wsdl.NESingleRequest;
import com.agency.nip.service.models.ft.wsdl.NESingleResponse;
import com.agency.nip.service.models.ft.wsdl.Nameenquirysingleitem;
import com.agency.nip.service.models.ft.wsdl.NameenquirysingleitemResponse;
import com.agency.nip.service.models.ft.wsdl.ObjectFactory;
import com.agency.nip.service.models.ft.wsdl.TSQuerySingleRequest;
import com.agency.nip.service.models.ft.wsdl.TSQuerySingleResponse;
import com.agency.nip.service.models.ft.wsdl.Txnstatusquerysingleitem;
import com.agency.nip.service.models.ft.wsdl.TxnstatusquerysingleitemResponse;
import com.agency.nip.service.models.nip.TransformedResponse;
import com.agency.nip.service.models.requests.FTRequest;
import com.agency.nip.service.models.responses.UserAccounts;
import com.agency.nip.service.repository.impl.DataService;
import com.agency.octopus.ssm.common.PGP_SSM;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.JAXBElement;
import lombok.extern.slf4j.Slf4j;
import org.inlaks.responsecode.InlaksResponseCodeEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author odavies
 */
@Slf4j
@Component
public class OutwardFinancialOperationsFacade extends BaseConfig {

    @Autowired
    private GeneralMapper generalMapper;

    @Autowired
    private DataService dataService;

    @Autowired
    private PGP_SSM pgp_ssm;

    @Autowired
    private XmlBuilder xmlBuilder;

    @Autowired
    private SOAPConnector processorService;

    ObjectFactory objectFactory = new ObjectFactory();

    public UserAccounts nameEnquiry(String code, String channel, String accountNumber) {
        Nameenquirysingleitem nameenquirysingleitem = objectFactory.createNameenquirysingleitem();
        log.info("REQUEST ::: " + xmlBuilder.convertObjectToXmlStringNoNS(generalMapper.generateNIPNameEnquiryRequest(channel, code, accountNumber), NESingleRequest.class));

        nameenquirysingleitem.setRequest(pgp_ssm.encrypt(xmlBuilder.convertObjectToXmlStringNoNS(generalMapper.generateNIPNameEnquiryRequest(channel, code, accountNumber), NESingleRequest.class)));

        log.info("ENCRYPTED REQUEST ::: " + xmlBuilder.convertObjectToXmlStringNoNS(objectFactory.createNameenquirysingleitem(nameenquirysingleitem), Nameenquirysingleitem.class));

        JAXBElement<NameenquirysingleitemResponse> nibssResponse = (JAXBElement<NameenquirysingleitemResponse>) processorService.callWebService(getFtBaseUrl(), objectFactory.createNameenquirysingleitem(nameenquirysingleitem));
        log.info("RESPONSE ::: " + pgp_ssm.decrypt(nibssResponse.getValue().getReturn()));
        NESingleResponse convertStringToObject = (NESingleResponse) xmlBuilder.convertStringToObject(pgp_ssm.decrypt(nibssResponse.getValue().getReturn()), NESingleResponse.class);
        InlaksResponseCodeEnum inlaksAgencyResponseMapFromCode = NIPResponseCodes.getInlaksAgencyResponseMapFromCode(convertStringToObject.getResponseCode());
        UserAccounts response = new UserAccounts();
        if (inlaksAgencyResponseMapFromCode.equals(InlaksResponseCodeEnum.RESPONSE_00)) {
            response.setAccountName(convertStringToObject.getAccountName());
            response.setBankVerificationNumber(convertStringToObject.getBankVerificationNumber());
        }
        response.setResponseCode(inlaksAgencyResponseMapFromCode.getResponseCode());
        response.setResponseDescription(inlaksAgencyResponseMapFromCode.getResponseDescription());
        response.setTransactionRef(convertStringToObject.getSessionID());
        return response;
    }

    public TransformedResponse<FTSingleCreditResponse> directCredit(FTRequest request) {
        TransformedResponse<FTSingleCreditResponse> response = new TransformedResponse();
        if (request.getNarration().length() > 100) {
            response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_45.getResponseCode());
            response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_45.getResponseDescription().concat(" narration length max 100"));
            response.setTransactionRef(request.getRequestId());
            return response;
        }

        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(request.getNarration());
        if (m.find()) {
            response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_45.getResponseCode());
            response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_45.getResponseDescription().concat(" narration can not contain special characters"));
            response.setTransactionRef(request.getRequestId());
            return response;
        }

        FundtransfersingleitemDc nibbssRequest = objectFactory.createFundtransfersingleitemDc();
        // save
        FundsTransferPayloadLogEntity payloadLog = new FundsTransferPayloadLogEntity();
        NIPFundsTransferEntity save = dataService.save(generalMapper.convertToFTRecord.apply(request, this.nameEnquiry(request.getDestinationInstitutionCode(), request.getChannelCode(), request.getDestinationAccountNumber())));

        // map
        FTSingleCreditRequest ftReq = new FTSingleCreditRequest();
        BeanUtils.copyProperties(save, ftReq);

        // log
        payloadLog.setRequestPayload(xmlBuilder.convertObjectToXmlStringNoNS(ftReq, FTSingleCreditRequest.class));
        payloadLog.setFundsTransactionEntity(save);
        save.setFundsTransferPayloadLogEntity(payloadLog);
        log.info("REQUEST ::: " + xmlBuilder.convertObjectToXmlStringNoNS(ftReq, FTSingleCreditRequest.class));

        // encrypt
        nibbssRequest.setRequest(pgp_ssm.encrypt(xmlBuilder.convertObjectToXmlStringNoNS(ftReq, FTSingleCreditRequest.class)));
        payloadLog.setRequestTime(new Date());
        JAXBElement<FundtransfersingleitemDcResponse> nibssResponse = (JAXBElement<FundtransfersingleitemDcResponse>) processorService.callWebService(getFtBaseUrl(), objectFactory.createFundtransfersingleitemDc(nibbssRequest));
        payloadLog.setResponseTime(new Date());
        if (nibssResponse != null) {
            String decrypt = pgp_ssm.decrypt(nibssResponse.getValue().getReturn());
            log.info("RESPONSE ::: " + decrypt);
            FTSingleCreditResponse convertStringToObject = (FTSingleCreditResponse) xmlBuilder.convertStringToObject(decrypt, FTSingleCreditResponse.class);
            InlaksResponseCodeEnum inlaksAgencyResponseMapFromCode = NIPResponseCodes.getInlaksAgencyResponseMapFromCode(convertStringToObject.getResponseCode());
            payloadLog.setResponsePayload(decrypt);
            switch (inlaksAgencyResponseMapFromCode) {
                case RESPONSE_00:
                    save.setProcessStatus('S');
                    save.setTransactionRef(convertStringToObject.getSessionID());
                    response.setNibssResponse(convertStringToObject);
                    break;
                case RESPONSE_35:
                    save.setProcessStatus('P');
                    save.setTransactionRef(convertStringToObject.getSessionID() != null ? convertStringToObject.getSessionID() : null);
                    break;
                default:
                    save.setProcessStatus('F');
                    break;
            }
            // update
            save.setResponseCode(convertStringToObject.getResponseCode());
            dataService.save(save);
            response.setResponseCode(inlaksAgencyResponseMapFromCode.getResponseCode());
            response.setResponseDescription(inlaksAgencyResponseMapFromCode.getResponseDescription());
            response.setTransactionRef(convertStringToObject.getSessionID());

        } else {
            response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_02.getResponseCode());
            response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_02.getResponseDescription());
            response.setTransactionRef(request.getRequestId());
        }
        return response;
    }

    public TransformedResponse tsq(String transactionRef) {
        TransformedResponse response = new TransformedResponse();
        NIPFundsTransferEntity find = dataService.find(transactionRef);
        if (find != null && (find.getProcessStatus() == 'I' || find.getProcessStatus() == 'P')) {
            Txnstatusquerysingleitem nibbssRequest = objectFactory.createTxnstatusquerysingleitem();
            TSQuerySingleRequest request = new TSQuerySingleRequest();
            request.setChannelCode(find.getChannelCode());
            request.setSessionID(find.getSessionID());
            request.setSourceInstitutionCode(generalMapper.getInstitutioncode());
            log.info("REQUEST ::: " + xmlBuilder.convertObjectToXmlStringNoNS(request, TSQuerySingleRequest.class));
            // encrypt
            nibbssRequest.setRequest(pgp_ssm.encrypt(xmlBuilder.convertObjectToXmlStringNoNS(request, TSQuerySingleRequest.class)));

            log.info("REQUEST ::: " + nibbssRequest.getRequest());

            JAXBElement<TxnstatusquerysingleitemResponse> nibssResponse = (JAXBElement<TxnstatusquerysingleitemResponse>) processorService.callWebService(getTsqBaseUrl(), objectFactory.createTxnstatusquerysingleitem(nibbssRequest));

            TSQuerySingleResponse convertStringToObject = (TSQuerySingleResponse) xmlBuilder.convertStringToObject(pgp_ssm.decrypt(nibssResponse.getValue().getReturn()), TSQuerySingleResponse.class);

            InlaksResponseCodeEnum inlaksAgencyResponseMapFromCode = NIPResponseCodes.getInlaksAgencyResponseMapFromCode(convertStringToObject.getResponseCode());
            find.getFundsTransferPayloadLogEntity().setResponsePayload(pgp_ssm.decrypt(nibssResponse.getValue().getReturn()));

            switch (inlaksAgencyResponseMapFromCode) {
                case RESPONSE_00:
                    find.setProcessStatus('S');
                    find.setTransactionRef(convertStringToObject.getSessionID());
                    response.setNibssResponse(convertStringToObject);
                    break;
                case RESPONSE_35:
                    find.setProcessStatus('P');
                    find.setTransactionRef(convertStringToObject.getSessionID() != null ? convertStringToObject.getSessionID() : null);
                    break;
                default:
                    find.setProcessStatus('F');
                    break;
            }
            // update
            find.setResponseCode(convertStringToObject.getResponseCode());
            dataService.save(find);
            response.setResponseCode(inlaksAgencyResponseMapFromCode.getResponseCode());
            response.setResponseDescription(inlaksAgencyResponseMapFromCode.getResponseDescription());
            response.setTransactionRef(convertStringToObject.getSessionID());

        } else if (find != null && find.getProcessStatus() == 'F') {
            response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_00.getResponseCode());
            response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_00.getResponseDescription());
            response.setTransactionRef(find.getRequestId());
        } else {
            response.setResponseCode(InlaksResponseCodeEnum.RESPONSE_00.getResponseCode());
            response.setResponseDescription(InlaksResponseCodeEnum.RESPONSE_00.getResponseDescription());
            response.setTransactionRef(find.getSessionID());
        }
        return response;
    }

}
