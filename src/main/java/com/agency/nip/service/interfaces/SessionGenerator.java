/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.interfaces;

import com.agency.nip.service.facade.HelperFacade;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author odavies
 */
public interface SessionGenerator {

    @PostConstruct
    @Autowired
    public void generateSessionID(HelperFacade helperFacade);
}
