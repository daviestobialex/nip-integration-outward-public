/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.interfaces;

import org.w3c.dom.Document;

/**
 *
 * @author odavies
 * @param <T>
 */
public interface XmlBuilderInterface<T extends Object> {

    public T convertStringToObject(String StringObject, Class<?> t);
    
    public T convertStringToObject2(String StringObject, Class<?> t);

    public String convertObjectToXmlString(T object, Class<T> frame);
    
    public String convertObjectToXmlStringNoNS(T object, Class<T> frame);
    
    public String convertObjectToXmlString(T object, Class<T> frame, String rootElementName);

    public Document convertObjectToDocument(T object, Class<T> frame);

    public Document convertStringToDocument(String xmlStr);

    public String toXmlString();
}
