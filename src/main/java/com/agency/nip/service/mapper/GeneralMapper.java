/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.mapper;

import com.agency.nip.service.config.BaseConfig;
import com.agency.nip.service.entity.NIPFundsTransferEntity;
import com.agency.nip.service.facade.HelperFacade;
import com.agency.nip.service.models.ft.wsdl.NESingleRequest;
import com.agency.nip.service.models.requests.FTRequest;
import com.agency.nip.service.models.responses.UserAccounts;
import java.util.function.BiFunction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author odavies
 */
@Slf4j
@Component
public class GeneralMapper extends BaseConfig {

    @Autowired
    private HelperFacade helperFacade;

    /**
     * Generates the name inquiry request going to NIBSS
     *
     * @param channelCode
     * @param institutionCode
     * @param accountNumber
     * @return
     */
    public NESingleRequest generateNIPNameEnquiryRequest(String channelCode, String institutionCode, String accountNumber) {
        NESingleRequest request = new NESingleRequest();
        request.setAccountNumber(accountNumber);
        request.setChannelCode(channelCode);
        request.setDestinationInstitutionCode(institutionCode);
        request.setSessionID(helperFacade.generateSessionID(getInstitutioncode()));
        return request;
    }

    public BiFunction<FTRequest, UserAccounts, NIPFundsTransferEntity> convertToFTRecord = (request, neResponse) -> {
        NIPFundsTransferEntity record = new NIPFundsTransferEntity();
        record.setAmount(request.getAmount());
        record.setOriginatorAccountNumber(request.getSourceAccountNumber());
        record.setDestinationInstitutionCode(request.getDestinationInstitutionCode());
        record.setBeneficiaryAccountNumber(request.getDestinationAccountNumber());
        record.setSessionID(helperFacade.generateSessionID(getInstitutioncode()));
        record.setBeneficiaryBankVerificationNumber(neResponse.getBankVerificationNumber());
        record.setOriginatorAccountName(null);
        record.setOriginatorBankVerificationNumber(null);
        record.setBeneficiaryAccountName(neResponse.getAccountName());
        record.setRequestId(request.getRequestId());
        record.setChannelCode(request.getChannelCode());
        record.setNarration(request.getNarration());
        record.setNameEnquiryRef(neResponse.getTransactionRef());
        record.setPaymentReference(request.getRequestId());
        return record;
    };

//    
//    public Function<AmountOrAccountBlockOrUnblockRequest, NIPAmountAcountBlockUnblockEntity> convertToBlockUnBlockRecord = request -> {
//        NIPAmountAcountBlockUnblockEntity record = new NIPAmountAcountBlockUnblockEntity();
//        BeanUtils.copyProperties(request, record);
//        return record;
//    };
}
