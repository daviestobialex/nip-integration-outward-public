/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.mapper;

import com.agency.nip.service.interfaces.XmlBuilderInterface;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.apache.commons.io.IOUtils;
import org.xml.sax.InputSource;
import java.io.StringReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.dom4j.dom.DOMDocument;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author ailori
 * @param <I>
 */
@Slf4j
@Component
public class XmlBuilder implements XmlBuilderInterface {

    @Override
    public Object convertStringToObject(String StringObject, Class frame) {
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(frame);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return jaxbUnmarshaller.unmarshal(new StreamSource(new StringReader(StringObject)), frame).getValue();
//            return jaxbUnmarshaller.unmarshal(IOUtils.toInputStream(StringObject, StandardCharsets.UTF_8));
        } catch (JAXBException e) {
            log.info(e.getMessage());
        }
        return null;
    }

    @Override
    public Object convertStringToObject2(String StringObject, Class frame) {
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(frame);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//            return jaxbUnmarshaller.unmarshal((Source) IOUtils.toInputStream(StringObject, StandardCharsets.UTF_8), frame).getValue();
            return jaxbUnmarshaller.unmarshal(IOUtils.toInputStream(StringObject, StandardCharsets.UTF_8));
        } catch (JAXBException e) {
            log.info(e.getMessage());
        }
        return null;
    }

    @Override
    public String convertObjectToXmlString(Object object, Class frame) {
        try {
            //Create JAXB Context and Create Marshaller
            Marshaller jaxbMarshaller = JAXBContext.newInstance(frame).createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(object, sw);
            return sw.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String convertObjectToXmlStringNoNS(Object object, Class frame) {
        try {

            //Create JAXB Context and Create Marshaller
            Marshaller jaxbMarshaller = JAXBContext.newInstance(frame).createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(object, sw);
//            return this.remove(sw.toString());
            return sw.toString();

        } catch (JAXBException e) {
            e.printStackTrace();
//        } catch (TransformerConfigurationException ex) {
//            Logger.getLogger(XmlBuilder.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (TransformerException | SAXException | ParserConfigurationException | TransformerFactoryConfigurationError | IOException ex) {
//            Logger.getLogger(XmlBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public String convertObjectToXmlString(Object object, Class frame, String rootelementName) {
        try {
            //Create JAXB Context and Create Marshaller
            Marshaller jaxbMarshaller = JAXBContext.newInstance(frame).createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
//            jaxbMarshaller.marshal(object, sw);
            jaxbMarshaller.marshal(new JAXBElement(new QName("http://core.nip.nibss/", rootelementName), frame, object), sw);
            return sw.toString();
//            return this.remove(sw.toString());

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Document convertObjectToDocument(Object object, Class frame) {
        try {
            //Create JAXB Context and Create Marshaller
            Marshaller jaxbMarshaller = JAXBContext.newInstance(frame).createMarshaller();

            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            //Print XML String to Console
            StringWriter sw = new StringWriter();

            //Write XML to StringWriter
            jaxbMarshaller.marshal(object, sw);
//            log.info("XML ::::: " + sw.toString());
            return convertStringToDocument(sw.toString());

        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            factory.setNamespaceAware(true);
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Document getDOMDocument(String responseXml) {
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        Document document;

        try {
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            document = builder.parse(new InputSource(new StringReader(responseXml)));
        } catch (Exception e) {
            document = new DOMDocument();
            Element errEl = document.createElement("XmlParseHelperError");
            errEl.appendChild(document.createTextNode(e.getMessage()));
            document.appendChild(errEl);
        }
        return document;
    }

    public Element getDOMElement(NodeList nL, String sName) {
        if (nL == null) {
            return null;
        }

        Element ret = null;
        for (int i = 0; i < nL.getLength(); i++) {
            Node n = nL.item(i);
            if (n.getNodeName().equalsIgnoreCase(sName)) {
                return (Element) n;
            } else if (n.getChildNodes().getLength() > 0) {
                ret = getDOMElement(n.getChildNodes(), sName);
            }
        }

        return ret;
    }

    public Element getDebugDOMElement(NodeList nL, int loop, String sName) {
        if (nL == null) {
            return null;
        }

        Element ret = null;
        for (int i = 0; i < nL.getLength(); i++) {
            Node n = nL.item(i);
            System.out.println(loop + ":: Node: " + n.getNodeName().trim() + " Type: " + n.getNodeType()
                    + "Value: " + n.getNodeValue() + "Children: " + n.getChildNodes().getLength());
            System.out.println("Content:" + n.getTextContent());
            if (n.getNodeName().equalsIgnoreCase(sName)) {
                return (Element) n;
            } else if (n.getChildNodes().getLength() > 0) {
                ret = getDebugDOMElement(n.getChildNodes(), ++loop, sName);
            }
        }

        return ret;
    }

    @Override
    public String toXmlString() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String remove(String xml) throws SAXException, IOException, ParserConfigurationException,
            TransformerFactoryConfigurationError, TransformerException {
        InputStream xmlData = IOUtils.toInputStream(xml);
        URL xsltURL = getClass().getClassLoader().getResource("removeNameSpace.xslt");
        Document xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlData);
        Source stylesource = new StreamSource(xsltURL.openStream(), xsltURL.toExternalForm());
        Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesource);
        StringWriter stringWriter = new StringWriter();
        transformer.transform(new DOMSource(xmlDocument), new StreamResult(stringWriter));
        return stringWriter.toString();
    }

}
