/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.inlaks.responsecode.InlaksResponseCodeEnum;

/**
 *
 * @author daviestobialex
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(value = "BaseResponse Model")
public class BaseResponse {

    @ApiModelProperty(value = "Transaction Reference  ", required = true)
    public String transactionRef;

    @ApiModelProperty(value = "Response Code  ", required = true)
    private String responseCode;

    @ApiModelProperty(value = "Response Description  ", required = true)
    private String responseDescription;

    @Temporal(TemporalType.TIMESTAMP)
    @ApiModelProperty(value = "Response time  ", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date responseTime;

    public BaseResponse(InlaksResponseCodeEnum responseCodeEnum) {
        this.responseCode = responseCodeEnum.getResponseCode();
        this.responseDescription = responseCodeEnum.getResponseDescription();
    }
}
