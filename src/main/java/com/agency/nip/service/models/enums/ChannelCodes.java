/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.models.enums;

import lombok.Getter;

/**
 *
 * @author odavies
 */
public enum ChannelCodes {

    _01("1", "Bank Teller"),
    _02("2", "Internet Banking"),
    _03("3", "Mobile Phones"),
    _04("4", "POS Terminals"),
    _05("5", "ATM"),
    _06("6", "Vendor/Merchant Web Portal"),
    _07("7", "Third – Party Payment Platform"),
    _08("8", "Unstructured Supplementary Service Data (USSD)"),
    _09("9", "Other Channels"),
    _10("10", "Social Media"),
    _11("11", "Agency Banking");

    @Getter
    private final String code;

    @Getter
    private final String description;

    ChannelCodes(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public static String getDescription(String reasonCode) {
        for (ReasonCodes resonCodesEnums : ReasonCodes.values()) {
            if (resonCodesEnums.getCode().equalsIgnoreCase(reasonCode)) {
                return resonCodesEnums.getDescription();
            }
        }
        return "Reason Code Given Could not Fetch Description";
    }
}
