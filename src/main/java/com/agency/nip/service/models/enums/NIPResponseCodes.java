/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.models.enums;

import lombok.Getter;
import org.inlaks.responsecode.InlaksResponseCodeEnum;

/**
 *
 * @author odavies
 */
public enum NIPResponseCodes {

    NIP_00("00", "Approved or completed successfully", InlaksResponseCodeEnum.RESPONSE_00),
    NIP_01("01", "Status unknown, please wait for settlement report", InlaksResponseCodeEnum.RESPONSE_29),
    NIP_03("03", "Invalid Sender", InlaksResponseCodeEnum.RESPONSE_30),
    NIP_05("05", "Do not honor", InlaksResponseCodeEnum.RESPONSE_31),
    NIP_06("06", "Dormant Account", InlaksResponseCodeEnum.RESPONSE_32),
    NIP_07("07", "Invalid Account", InlaksResponseCodeEnum.RESPONSE_33),
    NIP_08("08", "Account Name Mismatch", InlaksResponseCodeEnum.RESPONSE_34),
    NIP_09("09", "Request processing in progress", InlaksResponseCodeEnum.RESPONSE_35),
    NIP_12("12", "Invalid transaction", InlaksResponseCodeEnum.RESPONSE_36),
    NIP_13("13", "Invalid Amount", InlaksResponseCodeEnum.RESPONSE_37),
    NIP_14("14", "Invalid Batch Number", InlaksResponseCodeEnum.RESPONSE_36),
    NIP_15("15", "Invalid Session or Record ID", InlaksResponseCodeEnum.RESPONSE_38),
    NIP_16("16", "Unknown Bank Code", InlaksResponseCodeEnum.RESPONSE_92),
    NIP_17("17", "Invalid Channel", InlaksResponseCodeEnum.RESPONSE_40),
    NIP_18("18", "Wrong Method Call", InlaksResponseCodeEnum.RESPONSE_41),
    NIP_21("21", "No action taken", InlaksResponseCodeEnum.RESPONSE_42),
    NIP_25("25", "Unable to locate record", InlaksResponseCodeEnum.RESPONSE_43),
    NIP_26("26", "Duplicate record", InlaksResponseCodeEnum.RESPONSE_44),
    NIP_30("30", "Format error", InlaksResponseCodeEnum.RESPONSE_45),
    NIP_34("34", "Suspected fraud", InlaksResponseCodeEnum.RESPONSE_46),
    NIP_35("35", "Contact sending bank", InlaksResponseCodeEnum.RESPONSE_47),
    NIP_51("51", "No sufficient funds", InlaksResponseCodeEnum.RESPONSE_48),
    NIP_57("57", "Transaction not permitted to sender", InlaksResponseCodeEnum.RESPONSE_49),
    NIP_58("58", "Transaction not permitted on channel", InlaksResponseCodeEnum.RESPONSE_50),
    NIP_61("61", "Transfer limit Exceeded", InlaksResponseCodeEnum.RESPONSE_51),
    NIP_63("63", "Security violation", InlaksResponseCodeEnum.RESPONSE_52),
    NIP_65("65", "Exceeds withdrawal frequency", InlaksResponseCodeEnum.RESPONSE_53),
    NIP_68("68", "Response received too late", InlaksResponseCodeEnum.RESPONSE_54),
    NIP_69("69", "Unsuccessful Account/Amount block", InlaksResponseCodeEnum.RESPONSE_69),
    NIP_70("70", "Unsuccessful Account/Amount unblock", InlaksResponseCodeEnum.RESPONSE_70),
    NIP_71("71", "Empty Mandate Reference Number", InlaksResponseCodeEnum.RESPONSE_71),
    NIP_91("91", "Beneficiary Bank not available", InlaksResponseCodeEnum.RESPONSE_56),
    NIP_92("92", "Routing error", InlaksResponseCodeEnum.RESPONSE_57),
    NIP_94("94", "Duplicate transaction", InlaksResponseCodeEnum.RESPONSE_60),
    NIP_96("96", "System malfunction", InlaksResponseCodeEnum.RESPONSE_61),
    NIP_97("97", "Timeout waiting for response from destination", InlaksResponseCodeEnum.RESPONSE_63);

    @Getter
    private final String code;

    @Getter
    private final String description;

    @Getter
    private final InlaksResponseCodeEnum inlaksResponseCodeEnum;

    NIPResponseCodes(String code, String description, InlaksResponseCodeEnum inlaksResponseCodeEnum) {
        this.code = code;
        this.description = description;
        this.inlaksResponseCodeEnum = inlaksResponseCodeEnum;
    }

    public static InlaksResponseCodeEnum getInlaksAgencyResponseMapFromCode(String responseCode) {
        for (NIPResponseCodes nIPResponseCodes : NIPResponseCodes.values()) {
            if (nIPResponseCodes.getCode().equalsIgnoreCase(responseCode)) {
                return nIPResponseCodes.getInlaksResponseCodeEnum();
            }
        }
        return InlaksResponseCodeEnum.RESPONSE_17;
    }
}
