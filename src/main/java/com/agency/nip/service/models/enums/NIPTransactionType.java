/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.models.enums;

/**
 * This tells us if the nip transaction is either inward or outward
 *
 * @author odavies
 */
public enum NIPTransactionType {

    IN, OUT;
}
