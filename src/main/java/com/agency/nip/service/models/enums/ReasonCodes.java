/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.models.enums;

import lombok.Getter;

/**
 *
 * @author odavies
 */
public enum ReasonCodes {

    NIP_0001("0001", "Suspected fraud"),
    NIP_0002("0002", "Security violation"),
    NIP_0003("0003", "Multiple cases of insufficient fund"),
    NIP_0004("0004", "Multiple cases of Transfer limit Exceeded"),
    NIP_0005("0005", "Non-compliance with operating regulations"),
    NIP_0006("0006", "Identity theft"),
    NIP_0007("0007", "Duplicate transaction processing"),
    NIP_0008("0008", "Fraudulent multiple transactions"),
    NIP_0009("0009", "Payment made by other means"),
    NIP_0010("0010", "Purpose of payment not redeemed"),
    NIP_0011("0011", " Recurring transactions"),
    NIP_1111("1111", "Others");

    @Getter
    private final String code;

    @Getter
    private final String description;

    ReasonCodes(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public static String getDescription(String reasonCode) {
        for (ReasonCodes resonCodesEnums : ReasonCodes.values()) {
            if (resonCodesEnums.getCode().equalsIgnoreCase(reasonCode)) {
                return resonCodesEnums.getDescription();
            }
        }
        return "Reason Code Given Could not Fetch Description";
    }
}
