/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.models.nip;

import lombok.Data;

/**
 *
 * @author daviestobialex
 */
@Data
public class TransformedResponse<T extends Object> extends org.inlaks.dto.BaseResponse {

    private T nibssResponse;
}
