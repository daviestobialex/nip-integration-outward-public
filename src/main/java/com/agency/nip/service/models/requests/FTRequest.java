/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.models.requests;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.inlaks.dto.BaseRequest;

/**
 *
 * @author daviestobialex
 */
@Data
public class FTRequest extends BaseRequest {

    protected String narration;

    protected String sourceAccountNumber;

    protected String destinationAccountNumber;

    protected String sourceInstitutionCode;

    protected String destinationInstitutionCode;

    protected String amount;
    
    protected String channelCode;

    @JsonIgnore
    @Override
    public Long[] getRoleClaimId() {
        return super.getRoleClaimId(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
