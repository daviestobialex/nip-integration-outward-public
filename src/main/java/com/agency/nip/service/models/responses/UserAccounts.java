/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.models.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.inlaks.dto.BaseResponse;

/**
 *
 * @author odavies
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@lombok.Data
public class UserAccounts extends BaseResponse {

    private String accountNumber;

    private String productName;

    private String ledgerBalance;

    private String availableBalance;

    private String accountName;

    private String bankVerificationNumber;

    private String currency;
}
