/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.models.responses;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 *
 * @author odavies
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@lombok.Data
public class UserDetails {

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private String clientId;

    private String savingsId;
    
    private String productName;
}
