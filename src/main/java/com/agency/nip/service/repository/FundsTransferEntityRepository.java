/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.repository;

import com.agency.nip.service.entity.NIPFundsTransferEntity;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author odavies
 */
public interface FundsTransferEntityRepository extends CrudRepository<NIPFundsTransferEntity, Long> {

    public NIPFundsTransferEntity findByRequestIdOrTransactionRefOrSessionID(String requestId, String transRef, String SessionId);
}
