/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.repository;

import com.agency.nip.service.entity.FundsTransferPayloadLogEntity;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author daviestobialex
 */
public interface PayloadLoggerRepository extends CrudRepository<FundsTransferPayloadLogEntity, Long> {

}
