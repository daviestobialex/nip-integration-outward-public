/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.nip.service.repository.impl;

import com.agency.nip.service.entity.NIPFundsTransferEntity;
import com.agency.nip.service.repository.FundsTransferEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author odavies
 */
@Service
public class DataService {

    @Autowired
    private FundsTransferEntityRepository fundsTransferEntityRepository;

    public NIPFundsTransferEntity save(NIPFundsTransferEntity record) {
        return fundsTransferEntityRepository.save(record);
    }

    public NIPFundsTransferEntity find(String ref) {
        return fundsTransferEntityRepository.findByRequestIdOrTransactionRefOrSessionID(ref, ref, ref);
    }
}
