package com.agency.nip.service;

import com.agency.nip.service.config.BaseConfig;
import com.agency.nip.service.pgp.RSAKeyPairGenerator;
import com.agency.octopus.ssm.common.PGP_SSM;
import java.io.ByteArrayOutputStream;
import org.springframework.boot.test.context.SpringBootTest;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import org.bouncycastle.openpgp.PGPException;
import org.junit.jupiter.api.Test;

@Slf4j
@SpringBootTest
class ApplicationTests extends BaseConfig {

    private boolean isArmored = true;
    private String id = "inlaks";
    private String passwd = "inlaksr00ts1";
    private boolean integrityCheck = true;

//    private String pubKeyFile = "C:\\Users\\odavies\\Documents\\projects\\inlaks\\sanef account opening\\SANEF AO\\config\\pub.dat";
//    private String privKeyFile = "C:\\Users\\odavies\\Documents\\projects\\inlaks\\sanef account opening\\SANEF AO\\config\\pri.dat";
//    @Test
    public void genKeyPair() throws InvalidKeyException, NoSuchProviderException, SignatureException, IOException, PGPException, NoSuchAlgorithmException {

        RSAKeyPairGenerator rkpg = new RSAKeyPairGenerator();

        Security.addProvider(new BouncyCastleProvider());

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "BC");

        kpg.initialize(1024);

        KeyPair kp = kpg.generateKeyPair();

        FileOutputStream out1 = new FileOutputStream(getPrivatekey());
        FileOutputStream out2 = new FileOutputStream(getPublicKey());

        rkpg.exportKeyPair(out1, out2, kp.getPublic(), kp.getPrivate(), id, passwd.toCharArray(), isArmored);

    }

//    @Test
    public void encryptString() throws NoSuchProviderException, IOException, PGPException {
        PGP_SSM pgp_ssm = new PGP_SSM();
        InputStream finput = new URL("https://i.pinimg.com/564x/9c/b2/37/9cb2379f87717ca6ab483e1ad696d14f.jpg").openStream();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        int n = 0;
        byte[] buffer = new byte[1024];
        while (-1 != (n = finput.read(buffer))) {
            output.write(buffer, 0, n);
        }
        String imageStr = Base64.encodeBase64URLSafeString(output.toByteArray());
        String message = "{\n"
                + "    \"superagentCode\": \"AB0003\",\n"
                + "    \"agentCode\": \"004FJM01023\",\n"
                + "    \"bankCode\": \"000015\",\n"
                + "    \"requestId\": \"000001201910240846150999883774\",\n"
                + "    \"bankVerificationNumber\": \"22123456789\",\n"
                + "    \"firstName\": \"Samuel\",\n"
                + "    \"middleName\": \"John\",\n"
                + "    \"lastName\": \"Smith\",\n"
                + "    \"gender\": \"Male\",\n"
                + "    \"dateOfBirth\": \"1978-Oct-20\",\n"
                + "    \"houseNumber\": \"10B\",\n"
                + "    \"streetName\": \"Almond street\",\n"
                + "    \"city\": \"Igando\",\n"
                + "    \"lgaCode\": \"502\",\n"
                + "    \"emailAddress\": \"aa@gmail.com\",\n"
                + "    \"phoneNumber\": \"08012345678\",\n"
                + "    \"customerImage\": \"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABhAFoDASIAAhEBAxEB/8QAHQABAAEEAwEAAAAAAAAAAAAAAAcFBggJAgMEAf/EADcQAAEDAwIEBAMGBQUAAAAAAAEAAgMEBREGBxIhMUEIE1FhIjJxCSNCYoGRFBZDocEVM6Ph8P/EABwBAQACAgMBAAAAAAAAAAAAAAAEBgUHAQIIA//EAC0RAAEDAgQEBQQDAAAAAAAAAAEAAgMEEQUGIVESMUFhExQVcZEiobHBQoHw/9oADAMBAAIRAxEAPwDamiIiIiIiIiIiIiIiIiIiIiIiIiIiIqHrHW9g2+sc141JdqWzWyLAdUVcgYCT0a0dXOPZoyT2Cri1v7xbyw7k7u63ttROH1mlrnNbIIHnIiiacccYPTiIIcRzJbz5cKx9dV+Ti8QNvrZWXL+DHHa0Uofwi1yefx3Uua++0esVkmli0roe+6jY35aytAt8D/doeC8j6sCj2m+0/vktUGS6BooW55xmvfxfvw/4UBVNkvWvNQU9ksFFPdbpUu4Yqenblx9T6AAcyTyHdSI37Mnc+/UIqai/WCz1ThkU8k8sjmH8zmMIz9CfqokE887OO3wt3z5cyjl+IeokOeRoHONz3s3kO9lk1t/4+9LamkjivtkrrA53IzRvFTE364DXfs0rJDTeqbRq+2R3CzXCnuVG/pLTvDgD6HuD7HmtT2ofD9u34d5XS62srL1pVpwdR2V5qY6b3mbgPaz8zmgD1PaU9sNa3fRVfDcbJXOp3EAuDXZimb6OHRw/8FEnxSSieBO36fuq7UZSwXG6Z1VgMvC4fxJuPY3u4X3uR2WyNF4LDcHXayW+ucA11TTxzEN6AuaD/le9WUEOFwtHuaWOLTzCIiLldUREREWhjf65XbR/is3NqYpJKOtGo69wJHzRvnc5uR3BaWlb51gT43fBbU77Xaq3D29hik1ZTPdQ3e0Oe2M1nlcmSMccASBnAMEgObw4IIw6JUtD2hquGV6xlFXCSU2adL7G+l1A3hA8WNm2j1rXXLU1kmqYq+kFMaugDXywfGHEhriPhOBkZ7D6LPvT3jY2h1ExhZqZ1FI7+nWUczCPqQ0t/utRdTthrHRdW6lvul7vaZ2HhLauikj5+xI5/UK57BBNCWiSGRh7hzSMKEJTAyzVv+syjhWZj6hPI7xCALtcLGw00II+LLcRa96dvtRjyaTVtmqfMGPKfVsaXA9uFxH7KBt5/DpQaYim1No2BkdmJ82ptlOB5dOD1fCB0j7lo5N6jlyGIGnM/BzCnbazcq76Eq4/JqZKi1v+Gpt0ruKKVh+YcJ5A47hV6txKOdpgqWaHqOh3VZZk2bL83nMLnLiObXAWcNrjkdtOaza28kM2gtOPPV1upz/xtVwqn6fo6e3WOgpaMOFJDAyOEO6hgaA3P6YVQV0iaWxtaegC8/VDg+Z7m8iT+UREX1UdERERFBWr9aT7PbtVVZUskm09fI45p2MGTG9oDC9o9Rwgkdw76KdVZe6u3VPuPpqSieWxVsWZKWdw5Mfjofynof0PZYvEYpZIeKnP1tNx37f2FnMHqKaCp4KwXieOF3YGxuPYgFXJZrvb9RW2Gvt1TFWUc44mSxHIP/ft2XKssluuEbmVVBTVDD1bLC1w/uFgedTaz2P1BUQ0U89rq43/AH9HM3jhm9y08iCOjhz9CvbuF48tTyaIqLdbLJT2nUUo8s3OOUvjjbjm5kZHJ3pkkDrzUSkxWOobwyjhcOY/37V4lyDiLpWOw14kjdydcCwPU7ju299lDfj/ANf2Xbbe+kotBQUUL6Wjb/rNHDGP4Y1BcSG4bjheGYzw46jPPKrfhe1JTb+3G2U1DA+Co/iWxV9K45MDfmc7PdpaDg8umFiLqO13LU15LQ2oud0rp+gBkmnle79S5xJ+pytongF8Jc/h40bXXnUIb/N1/wCB89OCHCihaDwRZ7v+IlxHLoB0yY9Th0dc8G1tdfbqFesZq5cpULKV8xe7hIFzqTuNgD9tFldGxsbGtaMNAwAOwXJEVn5LziiIiIiIiIiIiIrO3G2qsO51rNLdqfE7QfJrIfhliPse49jyWGus/BTrSo1WLbbhSVltmJLLo+UMjjbn+o35gfZoP1WfaKBNQwzP8Qix7dfdW/Bs1YngbDHTOBYejtQDuNvweoUFbB+EfSOyL23TyxfNUOGHXWpjH3OerYW8+Ae/Nx9cclOqIprWhgsFX66vqsSndU1by956n9bDsNEREXZQERERERF0V9S6ioqiobDJUuijc8QxDL3kDPC33PREXeisuo3CrIaCWZmlbxLOJfLjhEB+8bk/HnHIYwcEZ549Su2TXtUKWomZpi8OMRLWtMP+5yfggDJwSwDOPxt9URXeitSHXM009Iz+XrtFHNK2OSSSnIETS2Q8ZAySAWAH3e3tzXGbXk8bWlmmbzKS5rS1tPjAIJyckdMAHGeftgkiu1FQrxqKqtsTnQWWtq+GSJmWAYc1wy5wAJd8PQ5A5+3MUVuvbs6SJp0vWND3hpdwyYaC97cn7v0Y130eO+ASK90VlR67u0sLHDS1WJHQukMbjIOFwgZKGkmLu5z48j8TfU4Fy2a4T3GCZ1RSOo5Yp3w8B4iHBpwHAua3II55Ax7lEVQREREREREXwd0REQdl9RERERERcfxLkiIiIiIi/9k=\",\n"
                + "    \"customerSignature\": \"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCABhAFoDASIAAhEBAxEB/8QAHQABAAEEAwEAAAAAAAAAAAAAAAcFBggJAgMEAf/EADcQAAEDAwIEBAMGBQUAAAAAAAEAAgMEBREGBxIhMUEIE1FhIjJxCSNCYoGRFBZDocEVM6Ph8P/EABwBAQACAgMBAAAAAAAAAAAAAAAEBgUHAQIIA//EAC0RAAEDAgQEBQQDAAAAAAAAAAEAAgMEEQUGIVESMUFhExQVcZEiobHBQoHw/9oADAMBAAIRAxEAPwDamiIiIiIiIiIiIiIiIiIiIiIiIiIiIqHrHW9g2+sc141JdqWzWyLAdUVcgYCT0a0dXOPZoyT2Cri1v7xbyw7k7u63ttROH1mlrnNbIIHnIiiacccYPTiIIcRzJbz5cKx9dV+Ti8QNvrZWXL+DHHa0Uofwi1yefx3Uua++0esVkmli0roe+6jY35aytAt8D/doeC8j6sCj2m+0/vktUGS6BooW55xmvfxfvw/4UBVNkvWvNQU9ksFFPdbpUu4Yqenblx9T6AAcyTyHdSI37Mnc+/UIqai/WCz1ThkU8k8sjmH8zmMIz9CfqokE887OO3wt3z5cyjl+IeokOeRoHONz3s3kO9lk1t/4+9LamkjivtkrrA53IzRvFTE364DXfs0rJDTeqbRq+2R3CzXCnuVG/pLTvDgD6HuD7HmtT2ofD9u34d5XS62srL1pVpwdR2V5qY6b3mbgPaz8zmgD1PaU9sNa3fRVfDcbJXOp3EAuDXZimb6OHRw/8FEnxSSieBO36fuq7UZSwXG6Z1VgMvC4fxJuPY3u4X3uR2WyNF4LDcHXayW+ucA11TTxzEN6AuaD/le9WUEOFwtHuaWOLTzCIiLldUREREWhjf65XbR/is3NqYpJKOtGo69wJHzRvnc5uR3BaWlb51gT43fBbU77Xaq3D29hik1ZTPdQ3e0Oe2M1nlcmSMccASBnAMEgObw4IIw6JUtD2hquGV6xlFXCSU2adL7G+l1A3hA8WNm2j1rXXLU1kmqYq+kFMaugDXywfGHEhriPhOBkZ7D6LPvT3jY2h1ExhZqZ1FI7+nWUczCPqQ0t/utRdTthrHRdW6lvul7vaZ2HhLauikj5+xI5/UK57BBNCWiSGRh7hzSMKEJTAyzVv+syjhWZj6hPI7xCALtcLGw00II+LLcRa96dvtRjyaTVtmqfMGPKfVsaXA9uFxH7KBt5/DpQaYim1No2BkdmJ82ptlOB5dOD1fCB0j7lo5N6jlyGIGnM/BzCnbazcq76Eq4/JqZKi1v+Gpt0ruKKVh+YcJ5A47hV6txKOdpgqWaHqOh3VZZk2bL83nMLnLiObXAWcNrjkdtOaza28kM2gtOPPV1upz/xtVwqn6fo6e3WOgpaMOFJDAyOEO6hgaA3P6YVQV0iaWxtaegC8/VDg+Z7m8iT+UREX1UdERERFBWr9aT7PbtVVZUskm09fI45p2MGTG9oDC9o9Rwgkdw76KdVZe6u3VPuPpqSieWxVsWZKWdw5Mfjofynof0PZYvEYpZIeKnP1tNx37f2FnMHqKaCp4KwXieOF3YGxuPYgFXJZrvb9RW2Gvt1TFWUc44mSxHIP/ft2XKssluuEbmVVBTVDD1bLC1w/uFgedTaz2P1BUQ0U89rq43/AH9HM3jhm9y08iCOjhz9CvbuF48tTyaIqLdbLJT2nUUo8s3OOUvjjbjm5kZHJ3pkkDrzUSkxWOobwyjhcOY/37V4lyDiLpWOw14kjdydcCwPU7ju299lDfj/ANf2Xbbe+kotBQUUL6Wjb/rNHDGP4Y1BcSG4bjheGYzw46jPPKrfhe1JTb+3G2U1DA+Co/iWxV9K45MDfmc7PdpaDg8umFiLqO13LU15LQ2oud0rp+gBkmnle79S5xJ+pytongF8Jc/h40bXXnUIb/N1/wCB89OCHCihaDwRZ7v+IlxHLoB0yY9Th0dc8G1tdfbqFesZq5cpULKV8xe7hIFzqTuNgD9tFldGxsbGtaMNAwAOwXJEVn5LziiIiIiIiIiIiIrO3G2qsO51rNLdqfE7QfJrIfhliPse49jyWGus/BTrSo1WLbbhSVltmJLLo+UMjjbn+o35gfZoP1WfaKBNQwzP8Qix7dfdW/Bs1YngbDHTOBYejtQDuNvweoUFbB+EfSOyL23TyxfNUOGHXWpjH3OerYW8+Ae/Nx9cclOqIprWhgsFX66vqsSndU1by956n9bDsNEREXZQERERERF0V9S6ioqiobDJUuijc8QxDL3kDPC33PREXeisuo3CrIaCWZmlbxLOJfLjhEB+8bk/HnHIYwcEZ549Su2TXtUKWomZpi8OMRLWtMP+5yfggDJwSwDOPxt9URXeitSHXM009Iz+XrtFHNK2OSSSnIETS2Q8ZAySAWAH3e3tzXGbXk8bWlmmbzKS5rS1tPjAIJyckdMAHGeftgkiu1FQrxqKqtsTnQWWtq+GSJmWAYc1wy5wAJd8PQ5A5+3MUVuvbs6SJp0vWND3hpdwyYaC97cn7v0Y130eO+ASK90VlR67u0sLHDS1WJHQukMbjIOFwgZKGkmLu5z48j8TfU4Fy2a4T3GCZ1RSOo5Yp3w8B4iHBpwHAua3II55Ax7lEVQREREREREXwd0REQdl9RERERERcfxLkiIiIiIi/9k=\",\n"
                + "    \"accountOpeningBalance\": 1000\n"
                + "}";
//        log.info(" BASE64 === " + imageStr);
        log.info(" ======= ENCRYPTED :::::: " + pgp_ssm.encrypt(message, getPublicKey()));
    }

//    @Test
    public void decryptString() throws NoSuchProviderException, IOException, PGPException {
        PGP_SSM pgp_ssm = new PGP_SSM();
        pgp_ssm.setPrivateKeyPassPhrase(passwd);
        String message = "2d2d2d2d2d4esc24547494e20504750204d4553534147452d2d2d2d2d0d0a56657273696f6e3a20424350472043232076312e382e312e300d0a0d0a68497744464779567a4e426c6f423442412f39714e62322f4269346d35384434464d39394d49654373664556324b357274416f6973374b5550384d32617666330d0a4a33347666696c4b54464972764b557431443348784f4d7a676650732b666630615158563050337839324e5459364737745545712f4659364b673371375155640d0a325242524d474a4149314478345a7a4631486831487172494f67646a516d61694662677848684f3331717876493338705757303153452f534a62512b6f744b510d0a415448446a5a787447386e33525276366d546f72695777716e2f722b4877614d572f6b386f386e48315449496e774a435654384757676c78536b624f7873616d0d0a4a48626c6a61586d71756b537a67624e6c5979386f79693731694c4976517755756d77764f2b76614a394c75314b572f6562477559614a43396c706a2f51735a0d0a3079546249485a43434f6a4d4d54766c335531486a386a347671766a6c76306d5a32532f2f6f37484151566c7144767042696177534d2b414238323644665a740d0a3d636a39750d0a2d2d2d2d2d454e4420504750204d4553534147452d2d2d2d2d0d0a\"";
        log.info(" ======= DECRYPT :::::: " + pgp_ssm.decrypt(message, getPrivatekey()));
    }
}
