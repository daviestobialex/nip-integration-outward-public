package com.agency.nip.service;

import com.agency.nip.service.facade.HelperFacade;
import com.agency.nip.service.mapper.XmlBuilder;
import com.agency.nip.service.models.ft.wsdl.NESingleRequest;
import com.agency.nip.service.models.ft.wsdl.Nameenquirysingleitem;
import com.agency.octopus.ssm.common.PGP_SSM;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
class NipServiceApplicationTests {

    @Test
    void contextLoads() {
        HelperFacade helperFacade = new HelperFacade();
        PGP_SSM pgp_ssm = new PGP_SSM();
        pgp_ssm.setPublicKeyLocation("/home/daviestobialex/Documents/projects/nip-integration-inward/config/Test_public.key");
        pgp_ssm.setPrivateKeyLocation("/home/daviestobialex/Documents/projects/nip-integration-inward/config/private-dev.asc");
        pgp_ssm.setPrivateKeyPassPhrase("inlaksr00ts1");
        XmlBuilder xmlBuilder = new XmlBuilder();
        Nameenquirysingleitem nameenquirysingleitem = new Nameenquirysingleitem();
        NESingleRequest request = new NESingleRequest();
        request.setAccountNumber("2005554348");
        request.setChannelCode("1");
        request.setSessionID(helperFacade.generateSessionID("999194"));
        request.setDestinationInstitutionCode("999057");
        log.info("BEFORE REQUEST ::: " + xmlBuilder.convertObjectToXmlStringNoNS(request, NESingleRequest.class));

        nameenquirysingleitem.setRequest(pgp_ssm.encrypt(xmlBuilder.convertObjectToXmlString(request, NESingleRequest.class)));
        log.info("REQUEST ::: " + xmlBuilder.convertObjectToXmlString(nameenquirysingleitem, Nameenquirysingleitem.class, "nameenquirysingleitem"));
    }

    @Test
    void decypt() {
        HelperFacade helperFacade = new HelperFacade();
        PGP_SSM pgp_ssm = new PGP_SSM();
        pgp_ssm.setPublicKeyLocation("/home/daviestobialex/Documents/projects/nip-integration-inward/config/Test_public.key");
        pgp_ssm.setPrivateKeyLocation("/home/daviestobialex/Documents/projects/nip-integration-inward/config/private-dev.asc");
        pgp_ssm.setPrivateKeyPassPhrase("inlaksr00ts1");
        XmlBuilder xmlBuilder = new XmlBuilder();

        log.info("DECRYPT ::: " + pgp_ssm.decrypt("848C03FEB08DE4381EAA2E0103FF413D33909465400F72BD0CA1035964FFE9FBCCFA4EE19C9D5F48034C2C847718371C798165F8E1B1C6B25423CF724D5CA8A07E8CC07F9B45AF50DE117D83F5A47E6586480137E138502FF8519276DF15BFDE13FDEB18DA565E8B14FE2C49EE33BAB897D1E3193ADA449CFFBDFBC64954DC71B7DEEC654DFFAD9D29A14F36A2C2C9C068ABB88B60EBFEDE5E49562A911205F50EDBAFED92C6DE5AF8FF63A4B422A20FCC47756FF966F85BAC4F23B5D670024A6C5D318033BA95357870CB65296CE140A5B30AC9FEB83303231F1A5D580ABCEAE55F6C85322266FA91671835174E576F2DE85C2FB4C8F95CCEA4E9DABD124FA2E95BA8F6A6788710D363C8EE70BB348C05E32C4D703FFE1F773C59F6AFC960C50A0F6E909D88D54D99D505D73A1243A549B7332333185197C0D648529AB8FDAB896048BFAA6E17F1AD2C74B57644F5D71FFAF305CF2F5DE03367ED6770911853375CD7665F2C7E694232CB7DE00382AA912FC6A15F87AAEB29D0184902350C0DB0DB1210BA5DFD56DED134F8B7112B6B8E052DF3E97E5F945FB5418BA78EE037395F65EAD91921EAB6446A092856F63E84D57D7A22293BE3DF"));
    }

    @Test
    public void generateSessionID() {
        HelperFacade f = new HelperFacade();
        log.info("999194");
        log.info(f.getCurrentDate("yyyyMMddHHmmss"));
        log.info(f.getRandomNumber(12));
        log.info("999194".concat(f.getCurrentDate("yymmddHHmmss")).concat(f.getRandomNumber(12)));
    }
}
